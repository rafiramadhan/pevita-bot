const generate = require('./generator')
const createApi = require('./api')

module.exports = async function deployer (config, deployments) {
    const api = createApi(config)
    console.log('Update bot')
    const bot = await generate(config)
    await Promise.all(
        deployments
            .map(id => api.deployBot(id, bot.version)
                .then(res => console.log(`Success deploy to ${id} version ${res.data.botVersion}`))))
        .then(() => console.log('Successfully deploy bot'))
        .catch(err => console.error('Error deploying bot: ', err))
}