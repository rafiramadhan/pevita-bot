const {group, slugify} = require('./util')
const _ = require('lodash')
const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

const util = {
    stringify: JSON.stringify,
    group,
    slugify
}

const productTemplate = _.template(fs.readFileSync(path.join(__dirname, './templates/product.yml')))

function createFlow (product, subProducts) {
    const resultYaml = productTemplate({
        product,
        subProducts,
        util
    })
    return yaml.safeLoad(resultYaml)
}

exports.createFlow = function (product, subProducts) {
    const flow = createFlow(product, subProducts)
    const flowName = 'product_' + slugify(product.title)
    return [flowName, flow]
}

exports.createNlu = function (products) {
    const productKeyword = products.map(p => [slugify(p.title), p.keyword])
        .reduce((agg, p) => Object.assign(agg, {[p[0]]: p[1]}), {})
    return {
        product: {
            type: 'keyword',
            options: {
               keywords: Object.assign({}, {
                    produk_utama: ["product", "produk"]
                }, productKeyword)
            }
        }
    }
}
