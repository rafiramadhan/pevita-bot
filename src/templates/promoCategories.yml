volatile: true
intents:
  initPayload:
    type: data
    initial: true
    condition: payload.menu == 'promoevent' && !payload.categoryId
  initPromo:
    type: command
    initial: true
    condition: content == 'gotoPromoFlow'
  initKeyword:
    type: text
    initial: true
    classifier:
      nlu: menu
      match: promoevent
  nextCategories:
    type: data
    condition: payload.eventType == 'nextCategories'
  noNextCategories:
    type: data
    condition: payload.eventType == 'noNextCategories'
  backToMainMenu:
    type: text
    classifier:
      nlu: menu
      match: main

states:
  init:
    initial: true
    transit: initContext
    transitions:
      displayingCategories:
        mapping:
          context.page: '1'
          context.carouselLimit: '10'
          context.buttonLimit: '10'
  displayingCategories:
    enter:
      context.carouselHasNext: context.page * context.carouselLimit < context.categoriesLength
      context.buttonHasNext: context.page * context.buttonLimit < context.categoriesLength
    action:
      - name: removeTimeOut
      - name: userTimeOut
      - name: info
        condition: context.page == 1
      - name: displayCategoriesCarousel
        condition: data.clientType == 'fbmessenger' || data.clientType == 'line'
      - name: displayCategoriesButton
        condition: data.clientType == 'telegram' || !data.clientType
      - name: askToMainMenu
        condition: >
          !((data.clientType == 'telegram' && context.buttonHasNext) || 
          ((data.clientType == 'fbmessenger' || data.clientType == 'line') && context.carouselHasNext))
      - name: askNextCategories
        condition: >
          (data.clientType == 'telegram' && context.buttonHasNext) || 
          (data.clientType == 'fbmessenger' && context.carouselHasNext)
      - name: askNextCategoriesLine
        condition: data.clientType == 'line' && context.carouselHasNext
    transit: initContext
    transitions:
      displayingCategories:
        condition: intent == 'nextCategories'
        mapping:
          context.page: context.page + 1
      endFlow:
        fallback: true
        condition: intent == 'noNextCategories'
  backToMainMenu:
    action:
      - name: backToMainMenu
    float:
      condition: intent == 'backToMainMenu'
      priority: 0
  endFlow:
    enter: initContext
    action:
      - name: removeTimeOut
      - name: userTimeOut
      - name: askToChoose
      - name: askBackToMain
    end: true

actions:
  askToChoose:
    type: text
    options:
      text: Ada lagi yang bisa $(config.botDisplayName) bantu hari ini? Kamu bisa pilih opsi menu di atas atau ketik layanan yang Kamu butuhkan
  askBackToMain:
    type: text
    options:
      text: Jika ingin kembali ke menu layanan Pegadaian, cukup ketik "Menu utama" ya
  displayCategoriesCarousel:
    type: method
    method: displayCategoriesCarousel
  askNextCategories:
    type: "template"
    options:
      type: "button"
      items:
        text: $(config.botDisplayName) masih punya informasi promo, event & artikel seputar $(config.bankName) lainnya nih. Ingin $(config.botDisplayName) tampilkan sekarang?
        # title: what?
        actions:
          - type: "postback"
            label: Ya
            payload: 
              eventType: nextCategories
          - type: "postback"
            label: Tidak
            payload: 
              eventType: noNextCategories
  askNextCategoriesLine:
    type: "template"
    options:
      type: "button"
      items:
        text: $(config.botDisplayName) masih punya informasi promo, event & artikel lainnya nih
        actions:
          - type: "postback"
            label: Lihat
            payload: 
              eventType: nextCategories
          - type: "postback"
            label: Nanti Saja
            payload: 
              eventType: noNextCategories
  askToMainMenu:
    type: text
    options:
      text: Jika ingin kembali ke menu utama, cukup ketik "Menu utama" ya.
  backToMainMenu:
    type: command
    options:
      command: gotoMenu
  displayCategoriesButton:
    type: method
    method: displayCategoriesButton
  userTimeOut:
    type: schedule
    options:
      id: userTimeOut
      command: add
      message:
        type: command
        content: userTimeOut
      start: $(data.parsedTime)
      end: $(data.parsedTimeEnd)
      freqType: minute
      freqInterval: 5
  removeTimeOut:
    type: schedule
    options:
      id: userTimeOut
      command: remove
  info:
    type: text
    options:
      text: "$(config.botDisplayName) tampilkan beberapa informasi Promo & Kegiatan $(config.bankName) yang kece untuk Kamu ya"

methods:
  initContext(ctx): >
    ctx.context.categories = ctx.config.promoCategories;
    ctx.context.categoriesLength = ctx.context.categories.length;

    let date = new Date(ctx.context.$now + 300000);
    let dateISO = date.toISOString();
    let parsedDate = dateISO.split('T');
    let parsedTime = parsedDate[1].split('.');
    ctx.data.parsedTime = parsedDate[0] + ' ' + parsedTime[0];

    let dateEnd = new Date(ctx.context.$now + 360000);
    let dateISOEnd = dateEnd.toISOString();
    let parsedDateEnd = dateISOEnd.split('T');
    let parsedTimeEnd = parsedDateEnd[1].split('.');
    ctx.data.parsedTimeEnd = parsedDateEnd[0] + ' ' + parsedTimeEnd[0];

    return ctx;

  displayCategoriesCarousel(message, context, data, options): >
    const start = (context.page - 1) * context.carouselLimit;
    const end = start + context.carouselLimit;
    const cat = context.categories.slice(start, end);
    const items = cat.map(c => ({
      title: c.name,
      text: c.description,
      thumbnailImageUrl: c.image,
      actions: [{
          type: 'postback',
          label: 'Pilih',
          payload: { 
              menu: 'promo',
              categoryId: c.id
          }
      }]
    }));
    return {
        type: 'data',
        payload: {
            type: 'template',
            template_type: 'carousel',
            items: items
        }
    };
  displayCategoriesButton(message, context, data, options): >
    const start = (context.page - 1) * context.buttonLimit;
    const end = start + context.buttonLimit;
    const categories = context.categories.slice(start, end);

    const actions = categories.map(category => ({
        type: 'postback',
        label: category.name,
        payload: {
          menu: 'promo',
          categoryId: category.id
        }
    }));

    return {
      type: 'data',
      payload: {
        type: 'template',
        template_type: 'button',
        items: {
          text: 'Kategori Promo',
          title: 'Kategori Promo',
          actions: actions
        }
      }
    };