// Only push no deploy
const generate = require('./generator')

module.exports = function push(config) {
    return generate(config)
        .then(bot => console.log(`Successfull update bot to version ${bot.version}`))
        .catch(err => {
            console.error('Error pushing bot: ', err)
            process.exit(1)
        })
}